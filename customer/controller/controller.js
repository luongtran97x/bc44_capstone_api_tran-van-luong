function render(res) {
  let contentHTML = "";
  for (let i = 0; i < res.data.length; i++) {
    let product = res.data[i];
    let contentDiv = `    <div class="products__grid-items">
    <a
      href="/customer/view/detail.html?productid=${i}"
      target="_blank"
      ><img
        src="${product.hinhAnh}"
        alt=""
        class="products__items-img inline-block"
    /></a>
    <h3 class="products__items-title">
      <a
        href="/customer/view/detail.html?productid=${i}"
        target="_blank"
        >${product.tenSP}</a
      >
    </h3>
    <p class="products__items-price">đ ${(product.giaSP).toLocaleString()}</p>
  </div>`;
    contentHTML += contentDiv;
  }
  document.getElementById("showProducts").innerHTML = contentHTML;
}

function batLoading(){
  document.getElementById("loading").style.display="flex";
}
function tatLoading(){
  document.getElementById("loading").style.display="none";
}