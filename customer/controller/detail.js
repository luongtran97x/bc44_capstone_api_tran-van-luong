const product_URL = "https://643d65076afd66da6af5d625.mockapi.io/products/";
const cart_URL = "https://643d65076afd66da6af5d625.mockapi.io/cart/";
const urlParams = new URLSearchParams(window.location.search);
const myParam = ~~urlParams.get("productid") + 1;
let daraCart = localStorage.getItem("CART_LOCAL");
let arrCart = [];
if (daraCart != null) {
  let data = JSON.parse(daraCart);
  for (let i = 0; i < data.length; i++) {
    let dataArr = data[i];
    let item = new products(
      dataArr.tenSP,
      dataArr.giaSP,
      dataArr.hinhAnh,
      dataArr.soLuong,
      dataArr.id
    );
    arrCart.push(item);
  }
  renderCart(arrCart);
  for (let i = 0; i < arrCart.length; i++) {
    let result = 0;
    result += arrCart[i].giaSP;
    document.querySelector(
      ".cart__item-total-price"
    ).innerHTML = `${result.toLocaleString()} đ`;
  }
  document.querySelector(".addToCart-item").innerHTML = data.length;
}

// Liên kết trang User với trang Detail
function linkPage() {
  batLoading();
  window.onload = function () {
    axios({
      url: `${product_URL}${myParam}`,
      method: "GET",
    })
      //lấy data từ api => render
      .then(function (res) {
        tatLoading();
        printItem(res);
        document.getElementById("btnAddToCart").onclick = function () {
          item = new products(
            res.data.tenSP,
            res.data.giaSP,
            res.data.hinhAnh,
            res.data.soLuong,
            res.data.id
          );
          arrCart.push(item);
          renderCart(arrCart);
          subTotal();
          showCart();
          postApi(arrCart);
          localStorage.setItem("CART_LOCAL", JSON.stringify(arrCart));
          tatLoading();
        };
      })

      .catch(function (err) {
        tatLoading();
      });
  };
}
linkPage();

// xóa sản phẩm trong giỏ hàng
function btnRemove(id) {
  let viTri = arrCart.findIndex(function (item) {
    return item.id == id;
  });
  arrCart.splice(viTri, 1);
  renderCart(arrCart);
  localStorage.setItem("CART_LOCAL", JSON.stringify(arrCart));
  showCart();
  subTotal();
}

// xóa giỏ hàng
function btnClearCart() {
  arrCart = [];
  renderCart(arrCart);
  localStorage.setItem("CART_LOCAL", JSON.stringify(arrCart));
  showCart();
  subTotal();
  postApi(arrCart);
}
// cập nhập giỏ hàng nếu không có sản phẩm
if (arrCart.length == 0) {
  document.querySelector(
    "#tbodyCart"
  ).innerHTML = `<h1 class="text-red-400 text-4xl text-center p-5">Oops! Your Car Is Empty!</h1>`;
  localStorage.setItem("CART_LOCAL", JSON.stringify(arrCart));
}

// tính tổng tiền trong giỏ hàng
function subTotal() {
  let subTotal = 0;
  arrCart.forEach(function (ele) {
    subTotal += ele.giaSP * ele.soLuong;
  });
  document.querySelector(
    ".cart__item-total-price"
  ).innerHTML = `${subTotal.toLocaleString()} đ`;
}
subTotal();

// Tăng  số lượng trong giỏ hàng
function btnPlus(id) {
  let viTri = arrCart.findIndex(function (item) {
    return item.id == id;
  });

  arrCart[viTri].soLuong++;
  renderCart(arrCart);

  localStorage.setItem("CART_LOCAL", JSON.stringify(arrCart));
  subTotal();
  showCart();
}

// Giảm số lượng sản phẩm trong giỏ hàng
function btnMinus(id) {
  let viTri = arrCart.findIndex(function (item) {
    return item.id == id;
  });
  if (arrCart[viTri].soLuong > 1) {
    arrCart[viTri].soLuong--;
  }

  renderCart(arrCart);
  localStorage.setItem("CART_LOCAL", JSON.stringify(arrCart));
  subTotal();
  showCart();
}
// hiển thị số lượng sản phẩm trong giỏ hàng
function showCart() {
  let show = 0;
  for (let i = 0; i < arrCart.length; i++) {
    show += arrCart[i].soLuong;
  }
  document.querySelector(".addToCart-item").innerHTML = show;
}
showCart();
function postApi(arrCart) {
  axios({
    url: cart_URL,
    method: "POST",
    data: arrCart,
  })
    .then(function (res) {})
    .catch(function (err) {});
}
function btnPayNow() {
  console.log(arrCart.length);
  if (arrCart.length == 0) {
    document.querySelector(".modal-payment--fail").style.display = "block";
  } else {
    document.querySelector(".modal-payment--success").style.display = "block";
  }
  btnClearCart();
}
