const BASE_URL = "https://644fc473ba9f39c6ab6bd8fc.mockapi.io/products";
var idSelect = null;
async function getProduct() {
  return await axios
    .get("https://644fc473ba9f39c6ab6bd8fc.mockapi.io/products")
    .then(function (res) {
      return res.data;
    });
}

async function searchProducts() {
  searchProduct = document.getElementById("search").value;
  var listPr = await getProduct();

  var productSearch = listPr.filter((value) => {
    return value.tenSP.toLowerCase().includes(searchProduct.toLowerCase());
  });
  console.log("productSearch: ", productSearch);
  document.getElementById("tbodySanPham").innerHTML = productSearch;
  renderProduct(productSearch);
}

function fetchDSSV() {
  // batLoading();
  svService
    .getList()
    //nếu thành công sẽ hiện ra bảng table
    .then(function (res) {
      // tatLoading();
      // gọi function renderProduct trong then() => nếu gọi ngoài then sẽ ko có dữ liệu đem đi render

      renderProduct(res.data);
    })
    // thất bại thì sẽ tắt loading
    .catch(function (err) {
      // tatLoading();
    });
}
fetchDSSV();
function themSinhVien() {
  var dataSV = layThongTinTuFrom();
  console.log("dataSV: ", dataSV);
  // axios({
  //   url: BASE_URL,
  //   method: "POST",
  //   data: dataSV,
  // })
  svService
    .create(dataSV)
    .then(function (res) {
      //gọi lại api lấy danh sách mới nhất từ sever sau khi xóa thành công
      fetchDSSV();
    })
    .catch(function (err) {});
}
function xoaSV(id) {
  console.log(id);
  // batLoading();
  // axios({
  //   url: `https://633ec05b0dbc3309f3bc5455.mockapi.io/sv/${id}`,
  //   method: "DELETE",
  // })
  svService
    .remove(id)
    .then(function (res) {
      fetchDSSV();
      console.log(res);
      Toastify({
        text: "Xoá sản phẩm thành công",
        offset: {
          x: 50, // horizontal axis - can be a number or a string indicating unity. eg: '2em'
          y: 10, // vertical axis - can be a number or a string indicating unity. eg: '2em'
        },
      }).showToast();
      // tatLoading();
    })
    .catch(function (err) {
      // tatLoading();
      console.log(err);
    });
}
function suaSV(id) {
  // batLoading();
  idSelect = id;
  axios({
    url: `https://644fc473ba9f39c6ab6bd8fc.mockapi.io/products/${id}`,
    method: "GET",
  })
    .then(function (res) {
      // tatLoading();
      showThongTinLenForm(res.data);

      // gọi function renderProduct trong then() => nếu gọi ngoài then sẽ ko có dữ liệu đem đi render
    })
    .catch(function (err) {
      // tatLoading();
      console.log("🚀 - file: demo.js:12 - err", err);
    });
}
function capNhatSanPham() {
  // batLoading();
  axios({
    url: `${BASE_URL}/${idSelect}`,
    method: "PUT",
    data: layThongTinTuFrom(),
  })
    .then(function (res) {
      // tatLoading();
      fetchDSSV();

      // gọi function renderProduct trong then() => nếu gọi ngoài then sẽ ko có dữ liệu đem đi render
    })
    .catch(function (err) {
      // tatLoading();
    });
}
